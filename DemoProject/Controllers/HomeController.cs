﻿using DemoProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DemoProject.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        Entities db = new Entities();
        public ActionResult Index()
        {
            var news = db.News.Where(d => d.isHidden == false).ToList();
            return View(news);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string email, string pwd)
        {
            Entities db = new Entities();
            var user = db.User.Where(d => d.Email == email && d.Password == pwd).FirstOrDefault();
            if (user != null)
            {
                return RedirectToAction("Index");
            }
            else {
                ViewBag.aaa = "error email";
            }
            return View();
        }
       
	}
}